<!DOCTYPE html>
<html>
<style>
	/* Стили таблицы (IKSWEB) */
table.iksweb {
	width: 100%;
	border: none;
	margin-bottom: 20px;
	border-collapse: separate;
}
table.iksweb thead th {
	font-weight: bold;
	text-align: left;
	border: none;
	padding: 10px 15px;
	background: #BDBDBD;
	font-size: 14px;
	color: #ffffff;
	border-top: 1px solid #ddd;
}
table.iksweb tr th:first-child, .table tr td:first-child {
	border-left: 1px solid #ddd;
}
table.iksweb tr th:last-child, .table tr td:last-child {
	border-right: 1px solid #ddd;
}
table.iksweb thead tr th:first-child {
	border-radius: 10px 0 0 0;
}
table.iksweb thead tr th:last-child {
	border-radius: 0 10px 0 0;
	text-align: center;
}
table.iksweb tbody td {
	text-align: left;
	border: none;
	padding: 10px 15px;
	font-size: 14px;
	vertical-align: top;
}
table.iksweb tbody tr:nth-child(even) {
	background: #F8F8F8;
}
table.iksweb tbody tr:last-child td{
	border-bottom: 1px solid #ddd;
}
table.iksweb tbody tr:last-child td:first-child {
	border-radius: 0 0 0 20px;
}
table.iksweb tbody tr:last-child td:last-child {
	border-radius: 0 0 20px 0;
}
</style>

<h1>Table</h1>
<table class="iksweb">
<thead>
<tr>
	<th width="25%">Field</th>
	<th width="25%">Type</th>
	<th width="50%">Description</th>
</tr>
</thead>
<tbody>
<tr>
	<td>Name</td>
	<td>First</td>
	<td>Here, you can usually find detailed description.</td>
</tr>
<tr>
	<td>Age</td>
	<td>Maximum</td>
	<td>Here, you can usually find detailed description.</td>
</tr>
<tr>
	<td>Skill</td>
	<td>Medium</td>
	<td>Here, you can usually find detailed description.</td>
</tr>
<tr>
	<td>Experience</td>
	<td>Great</td>
	<td>Here, you can usually find detailed description.</td>
</tr>
<tr>
	<td>Result</td>
	<td>Best</td>
	<td>Here, you can usually find detailed description.</td>
</tr>

</tbody>
</table>


<style>
div {
  margin-bottom: 15px;
  padding: 10px 5px 5px 10px;
}
.danger {
  background-color: #ffdddd;
  border-left: 6px solid #f44336;
}

.success {
  background-color: #ddffdd;
  border-left: 6px solid #4CAF50;
}

.info {
  background-color: #e7f3fe;
  border-left: 6px solid #2196F3;
}


.warning {
  background-color: #ffffcc;
  border-left: 6px solid #ffeb3b;
}
</style>
</head>
<body>
<h1>Note Types</h1>

<div class="danger">
  <p><strong>Mind the following:</strong></p>
  <p>
	<ul>
    <li>The first rule is you do not talk about Fight Club.</li>
    <li>The second rule is you do NOT talk about Fight Club.</li>
    <li>If this is your first night at Fight Club, you have to fight.</li>
	</ul>
  </p>
</div>

<div class="success">
  <p><strong>TIP</strong></p>
  <p>Did you know if you mix gasoline and frozen orange juice, you can make napalm?</p>
</div>

<div class="info">
  <p><strong>Information</strong></p>
  <p>All work and no sleep make Jack a dangerous boy.</p>
</div>

<div class="warning">
  <p><strong>Warning!</strong></p>
  <p>I am Jack's colon. If I get cancer, I'll kill Jack.</p>
</div>

</body>

</html>
